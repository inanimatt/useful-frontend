import Vue from 'vue';

export const getAll = () => Vue.http({
  method: 'GET',
  url: 'api/tags',
}).then((response) => response.data.data.map((tag) => tag.tag));
