import { router } from '../main';

export default {

  // A vue-router interceptor to inject authorisation headers into api requests
  authInterceptor: {
    request(request) {
      const token = localStorage.getItem('id_token');
      if (token && request.url.indexOf('api') === 0) {
        // Return a new request object with the authorisation headers injected
        return Object.assign(request, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
      }

      return request;
    },
    response(response) {
      // Logout and redirect to login page if 401 error
      if (response.status === 401 && response.request.url !== 'login_check') {
        localStorage.removeItem('id_token');
        router.go({ name: 'loggedout' });
      }

      return response;
    },
  },
};
