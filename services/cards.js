import Vue from 'vue';

const addSingleCardUseful = (response) => {
  const cardData = response.data.data;
  cardData.markedUsefulByUser = response.data.meta.markedUseful;

  return cardData;
};


const addCollectionUseful = (response) => {
  const newResponse = response;

  newResponse.data.data = newResponse.data.data.map((card) => {
    const cardWithUseful = card;
    cardWithUseful.markedUsefulByUser = newResponse.data.meta.markedUseful.includes(card.id);

    return cardWithUseful;
  });

  return newResponse.data;
};


export const getLatest = (limit = 10, page = 1) => Vue.http({
  method: 'GET',
  url: 'api/cards',
  params: {
    sort: 'date',
    limit,
    page,
    include: 'owner',
  },
}).then((response) => addCollectionUseful(response));


export const getOwn = (limit = 10, page = 1) => Vue.http({
  method: 'GET',
  url: 'api/profile/cards',
  params: {
    limit,
    page,
    include: 'owner',
  },
}).then((response) => addCollectionUseful(response));


export const getUseful = (limit = 10, page = 1) => Vue.http({
  method: 'GET',
  url: 'api/profile/useful',
  params: {
    limit,
    page,
    include: 'owner',
  },
}).then((response) => addCollectionUseful(response));


export const getCard = id => Vue.http({
  method: 'GET',
  url: `api/cards/${id}`,
  params: {
    include: 'owner',
  },
}).then(addSingleCardUseful);


export const getByTag = (tag, limit = 10, page = 1) => Vue.http({
  method: 'GET',
  url: 'api/cards/_byTag',
  params: {
    include: 'owner',
    tag,
    limit,
    page,
  },
}).then((response) => addCollectionUseful(response));


export const getSuggestions = (limit = 10, page = 1) => Vue.http({
  method: 'GET',
  url: 'api/profile/suggestions',
  params: {
    include: 'owner',
    limit,
    page,
  },
}).then((response) => addCollectionUseful(response));


export const search = (query, limit = 10, page = 1) => Vue.http({
  method: 'GET',
  url: 'api/cards/_search',
  params: {
    include: 'owner',
    q: query,
    limit,
    page,
  },
}).then((response) => addCollectionUseful(response));


export const createCard = (cardData) => Vue.http({
  method: 'POST',
  url: 'api/cards/',
  data: cardData,
}).then(addSingleCardUseful);


export const updateCard = (cardData) => Vue.http({
  method: 'PATCH',
  url: `api/cards/${cardData.id}`,
  data: cardData,
  params: {
    include: 'owner',
  },
}).then(addSingleCardUseful);


export const deleteCard = (id) => Vue.http({
  method: 'DELETE',
  url: `api/cards/${id}`,
});


export const markAsUseful = (cardId) => Vue.http({
  method: 'POST',
  url: 'api/profile/useful',
  data: {
    card: cardId,
  },
  params: {
    include: 'owner',
  },
}).then(addSingleCardUseful);


export const unmarkAsUseful = (cardId) => Vue.http({
  method: 'DELETE',
  url: `api/profile/useful/${cardId}`,
});
