import Vue from 'vue';

export const getProfile = () => Vue.http({
  method: 'GET',
  url: 'api/profile',
}).then((response) => response.data.data);

export const changePassword = (oldPw, newPw) => Vue.http({
  method: 'PUT',
  url: 'api/profile/password',
  data: {
    oldPw,
    newPw,
  },
}).then((response) => response.data.data);

export const createUser = (username, password) => Vue.http({
  method: 'POST',
  url: 'api/users',
  data: {
    username,
    password,
  },
}).then((response) => response.data.data);

export const listUsers = () => Vue.http({
  method: 'GET',
  url: 'api/users',
}).then((response) => response.data.data);

export const login = (username, password) => Vue.http({
  method: 'POST',
  url: 'login',
  data: {
    username,
    password,
  },
}).then((response) => response.data.token);
