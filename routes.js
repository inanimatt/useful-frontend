export default {
  '/': {
    component: require('./views/home.vue'),
    name: 'home',
    auth: true,
  },
  'cards/:id': {
    component: require('./views/view-card.vue'),
    name: 'view-card',
    auth: true,
  },
  'cards/': {
    component: require('./views/latest.vue'),
    name: 'latest',
    auth: true,
  },
  'my-cards/': {
    component: require('./views/own.vue'),
    name: 'my-cards',
    auth: true,
  },
  'useful/': {
    component: require('./views/useful.vue'),
    name: 'useful',
    auth: true,
  },
  'suggestions/': {
    component: require('./views/suggestions.vue'),
    name: 'suggestions',
    auth: true,
  },
  'tag/:tag': {
    component: require('./views/tag.vue'),
    name: 'tag',
    auth: true,
  },
  'search/': {
    component: require('./views/search.vue'),
    name: 'search',
    auth: true,
  },
  'login/': {
    component: require('./views/login.vue'),
    name: 'login',
    auth: false,
  },
  'account/': {
    component: require('./views/account.vue'),
    name: 'account',
    auth: true,
  },
  'error/404': {
    component: require('./views/404.vue'),
    name: 'not-found',
    auth: false,
  },
  'error/session-expired': {
    component: require('./views/loggedout.vue'),
    name: 'loggedout',
    auth: false,
  },
};
