/* global __DEV__ */
import Vue from 'vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';
import app from './app.vue';
import routes from './routes';
import auth from './services/auth.js';

Vue.use(VueRouter);
Vue.use(VueResource);

// Set the root of this app - equivalent to "<base href="">"
Vue.http.options.root = '';

// Automatically add authorisation token to all API requests
Vue.http.interceptors.push(auth.authInterceptor);

// Set up devtools
if (__DEV__) {
  window.VueDev = Vue;
}

// Create the router
export const router = new VueRouter({
  history: true,
  mode: 'html5',
});
router.redirect({
  '*': { name: 'not-found' }, // Redirect to 404 later
});
router.map(routes);

// Check if logged in before each page load
router.beforeEach((transition) => {
  // Load logged in state on startup, if it exists
  if (transition.to.auth && !localStorage.getItem('id_token')) {
    transition.redirect({ name: 'login', query: { backTo: transition.to.path } });
  } else {
    transition.next();
  }
});

// Start the app on the element with id `app`
router.start(app, '#app');
