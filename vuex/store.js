import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

// Create the initial state and define the mutations which can occur to the state.
// Mutations must be fully synchronous (i.e. no AJAX calls - do those in actions.js
// then dispatch a mutation with the result)

function initialCardListState() {
  return {
    data: [],
    meta: {
      page: 1,
      limit: 24,
      count: 0,
      nextPage: null,
      previousPage: null,
    },
  };
}

// Create the store
const store = new Vuex.Store({
  state: {
    tags: {
      all: [],
    },
    latest: initialCardListState(),
    useful: initialCardListState(),
    currentTag: initialCardListState(),
    suggestions: initialCardListState(),
    own: initialCardListState(),
    searchResults: initialCardListState(),
    currentCard: {
      title: '',
      content: '',
      owner: {
        data: { username: null },
      },
      tags: [],
      lastMarkedUseful: '',
      created: '',
      markedUsefulByUser: false,
    },
    cardEditor: {
      modalVisible: false,
    },
    user: {
      // Set initial state based on existence of token
      authenticated: !!localStorage.getItem('id_token'),
      profile: {},
      password: {
        old: '',
        new1: '',
        new2: '',
      },
    },
  },
  mutations: {
    /* This linter rule would be fine elsewhere, maybe, but it's literally the point of mutations */
    /* eslint no-param-reassign: ["error", { "props": false }]*/
    SET_CURRENT_CARD(state, data) {
      state.currentCard = data;
    },
    UPDATE_CARDS(state, { type, data }) {
      state[type].data = data;
    },
    UPDATE_CARDS_META(state, { type, data }) {
      state[type].meta = Object.assign(state[type].meta, data);
    },
    UPDATE_GLOBAL_TAG_LIST(state, tags) {
      state.tags.all = tags;
    },
    SET_CURRENT_CARD_TAGS(state, tags) {
      state.currentCard.tags = tags;
    },
    SET_CURRENT_CARD_CONTENT(state, content) {
      state.currentCard.content = content;
    },
    SET_CURRENT_CARD_TITLE(state, title) {
      state.currentCard.title = title;
    },
    TOGGLE_CURRENT_CARD_TAG(state, tag) {
      const currentTags = state.currentCard.tags;

      if (currentTags.includes(tag)) {
        state.currentCard.tags = currentTags.filter(value => value !== tag);
      } else {
        state.currentCard.tags.push(tag);
      }
    },
    CLEAR_CURRENT_CARD(state) {
      state.currentCard.content = '';
      state.currentCard.owner.first = '';
      state.currentCard.tags = [];
      state.currentCard.title = '';
    },
    SET_USER_PROFILE(state, profile) {
      state.user.profile = profile;
    },
    SET_EDIT_MODAL_VISIBILITY(state, visible) {
      state.cardEditor.modalVisible = !!visible;
    },
    UPDATE_PASSWORD(state, { type, password }) {
      state.user.password[type] = password;
    },
    SET_IS_AUTHENTICATED(state, authenticated) {
      state.user.authenticated = !!authenticated;
    },
  },
});

export default store;
