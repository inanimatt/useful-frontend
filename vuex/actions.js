import * as cards from '../services/cards.js';
import * as tags from '../services/tags.js';
import * as user from '../services/user.js';

// TODO: break these up when there are too many

export const loadLatest = (store, limit = 10, page = 1) => cards.getLatest(limit, page)
.then((cardData) => {
  store.dispatch({
    type: 'UPDATE_CARDS',
    payload: {
      type: 'latest',
      data: cardData.data,
    },
  });

  store.dispatch('UPDATE_CARDS_META', {
    type: 'latest',
    data: cardData.meta,
  });
});


export const loadSuggestions = (store, limit = 10, page = 1) => cards.getSuggestions(limit, page)
.then((cardData) => {
  store.dispatch({
    type: 'UPDATE_CARDS',
    payload: {
      type: 'suggestions',
      data: cardData.data,
    },
  });

  store.dispatch('UPDATE_CARDS_META', {
    type: 'suggestions',
    data: cardData.meta,
  });
});


export const loadUseful = (store, limit = 10, page = 1) => cards.getUseful(limit, page)
.then((cardData) => {
  store.dispatch({
    type: 'UPDATE_CARDS',
    payload: {
      type: 'useful',
      data: cardData.data,
    },
  });

  store.dispatch('UPDATE_CARDS_META', {
    type: 'useful',
    data: cardData.meta,
  });
});


export const loadOwn = (store, limit = 10, page = 1) => cards.getOwn(limit, page)
.then((cardData) => {
  store.dispatch({
    type: 'UPDATE_CARDS',
    payload: {
      type: 'own',
      data: cardData.data,
    },
  });

  store.dispatch('UPDATE_CARDS_META', {
    type: 'own',
    data: cardData.meta,
  });
});


export const loadByTag = (store, tag, limit = 10, page = 1) => cards.getByTag(tag, limit, page)
.then((cardData) => {
  store.dispatch({
    type: 'UPDATE_CARDS',
    payload: {
      type: 'currentTag',
      data: cardData.data,
    },
  });

  store.dispatch('UPDATE_CARDS_META', {
    type: 'currentTag',
    data: cardData.meta,
  });
});


export const loadCard = (store, cardId) => cards.getCard(cardId)
.then((cardData) => store.dispatch('SET_CURRENT_CARD', cardData));

export const setCurrentCardContent = ({ dispatch }, e) => {
  dispatch('SET_CURRENT_CARD_CONTENT', e.target.value);
};

export const setCurrentCardTitle = ({ dispatch }, e) => {
  dispatch('SET_CURRENT_CARD_TITLE', e.target.value);
};

export const setCurrentCardTags = ({ dispatch }, e) => {
  dispatch('SET_CURRENT_CARD_TAGS', e.target.value
    .split(/,(?:\s*)/)
    .map(value => value.trim())
    .filter(value => value)
  );
};

export const toggleTag = ({ dispatch }, tag) => {
  dispatch('TOGGLE_CURRENT_CARD_TAG', tag);
};

export const updateGlobalTagList = ({ dispatch }) => {
  tags.getAll().then(tagData => dispatch('UPDATE_GLOBAL_TAG_LIST', tagData));
};

export const clearCurrentCard = ({ dispatch }) => dispatch('CLEAR_CURRENT_CARD');

export const createCard = ({ state, dispatch }) => cards.createCard(state.currentCard)
.then((cardData) => {
  dispatch('CLEAR_CURRENT_CARD');

  return cardData;
});

export const updateCard = ({ state, dispatch }) => cards.updateCard(state.currentCard)
.then((cardData) => {
  dispatch('SET_CURRENT_CARD', cardData);

  return cardData;
});

export const loadProfile = ({ dispatch }) => {
  user.getProfile().then((profile) => dispatch('SET_USER_PROFILE', profile));
};

export const hideEditModal = ({ dispatch }) => dispatch('SET_EDIT_MODAL_VISIBILITY', false);

export const login = ({ dispatch }, { username, password }) => user.login(username, password)
.then((token) => {
  localStorage.setItem('id_token', token);
  dispatch('SET_IS_AUTHENTICATED', true);
});

export const logout = ({ dispatch }) => {
  localStorage.removeItem('id_token');
  dispatch('SET_IS_AUTHENTICATED', false);
};
